TXT+="\n╔═════════════════════════════════════════════╗"
TXT+="\n║ Texera UserBot Kurulumuna Hoşgeldiniz"   
TXT+="\n║      Telegram: @TexeraUserBot"
TXT+="\n╚═════════════════════════════════════════════╝"
TXT+="\n\n\n"
pkg update -y
rm -rf texera_installer
clear
echo -e $TXT
echo "Python Yükleniyor ⌛"
pkg install python -y
clear
echo -e $TXT
echo "Git Yükleniyor ⌛"
pkg install git -y
clear
echo -e $TXT
echo "Pyrogram Yükleniyor ⌛"
pip install pyrogram tgcrypto
echo "Repo klonlanıyor... ⌛"
git clone https://github.com/sherlock-exe/TexeraInstaller
clear
echo -e $TXT
cd TexeraInstaller
clear
echo "Gereksinimler Yükleniyor ⌛"
echo -e $TXT
pip install -r requirements.txt
clear
python -m texera_installer
